using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurpleOrb : MonoBehaviour {
    
    void OnTriggerEnter (Collider collider) {
        if(collider.gameObject.tag == "Player") {
            collider.gameObject.SendMessage("IncreaseHover", 0.1f);
            Destroy(gameObject);
        }
	}
}
