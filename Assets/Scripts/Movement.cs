using UnityEngine;

public class Movement : MonoBehaviour {

	[SerializeField, Range(0f, 100f)]
	float maxSpeed = 10f;

	[SerializeField, Range(0f, 100f)]
	float maxAcceleration = 60f, maxAirAcceleration = 15f, maxHoverAcceleration = 30f;

	[SerializeField, Range(0f, 10f)]
	float jumpSpeed = 8f;
	
	[SerializeField, Range(0f, 10f)]
	float hoverSpeed = 1.5f;

	[SerializeField, Range(0, 90)]
	float maxGroundAngle = 40f;

	[SerializeField, Range(0f, 100f)]
	float maxSnapSpeed = 100f;

	[SerializeField, Min(0f)]
	float probeDistance = 2f;

	[SerializeField, Min(0f)]
	float maxJumpTime = 0.2f;

	[SerializeField, Min(0f)]
	float maxHoverLength = 1f;

	public Transform cam;
	public ParticleSystem particles;

	Rigidbody body;

	Vector3 respawnPosition;

	Vector3 velocity, desiredVelocity;

	bool jumpPressed;
	bool hoverPressed;

	Vector3 contactNormal, wallNormal;

	int groundContactCount;

	bool OnGround => groundContactCount > 0;
	bool onWall = false;

	float minGroundDotProduct;

	int stepsSinceLastGrounded, stepsSinceLastJump;

	float timeJumpHeld, hoverLength;

	void OnValidate () {
		minGroundDotProduct = Mathf.Cos(maxGroundAngle * Mathf.Deg2Rad);
	}

	void Awake () {
		body = GetComponent<Rigidbody>();
		respawnPosition = gameObject.transform.position;
		OnValidate();
	}

	void Update () {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

		if(direction.magnitude >= 0.1f) {
			float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
			Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
			desiredVelocity = moveDir * maxSpeed;
		} else {
			desiredVelocity = Vector3.zero;
		}

		jumpPressed = Input.GetButton("Jump");
		hoverPressed = Input.GetButton("Fire3");
	}

	void FixedUpdate () {
		UpdateState();
		AdjustVelocity();

		if(jumpPressed) {
			Jump();
		} else if(OnGround) {
			timeJumpHeld = 0;
		} else {
			timeJumpHeld = maxJumpTime;
		}

		if(hoverPressed) {
			Hover();
		} else if(particles.isPlaying) {
			particles.Stop();
		}

		body.velocity = velocity;
		ClearState();
	}

	void ClearState () {
		groundContactCount = 0;
		contactNormal = Vector3.zero;
		onWall = false;
		wallNormal = Vector3.zero;
	}

	void UpdateState () {
		stepsSinceLastGrounded += 1;
		stepsSinceLastJump += 1;
		velocity = body.velocity;
		if (OnGround || SnapToGround()) {
			hoverLength = 0;
			stepsSinceLastGrounded = 0;
			if (groundContactCount > 1) {
				contactNormal.Normalize();
			}
		}
		else {
			contactNormal = Vector3.up;
		}
	}

	bool SnapToGround () {
		if (stepsSinceLastGrounded > 1 || stepsSinceLastJump <= 2) {
			return false;
		}
		float speed = velocity.magnitude;
		if (speed > maxSnapSpeed
            || !Physics.Raycast(body.position, Vector3.down, out RaycastHit hit, probeDistance)
            || hit.normal.y < minGroundDotProduct) {
			return false;
		}

		groundContactCount = 1;
		contactNormal = hit.normal;
		float dot = Vector3.Dot(velocity, hit.normal);
		if (dot > 0f) {
			velocity = (velocity - hit.normal * dot).normalized * speed;
		}
		return true;
	}

	void AdjustVelocity () {
		Vector3 xAxis = ProjectOnContactPlane(Vector3.right).normalized;
		Vector3 zAxis = ProjectOnContactPlane(Vector3.forward).normalized;

		float currentX = Vector3.Dot(velocity, xAxis);
		float currentZ = Vector3.Dot(velocity, zAxis);

		float acceleration;
		if(OnGround) {
			acceleration = maxAcceleration;
		} else if(hoverPressed && hoverLength < maxHoverLength) {
			acceleration = maxHoverAcceleration;
		} else {
			acceleration = maxAirAcceleration;
		}
		float maxSpeedChange = acceleration * Time.deltaTime;

		float newX = Mathf.MoveTowards(currentX, desiredVelocity.x, maxSpeedChange);
		float newZ = Mathf.MoveTowards(currentZ, desiredVelocity.z, maxSpeedChange);

		velocity += xAxis * (newX - currentX) + zAxis * (newZ - currentZ);
	}

	void Jump () {
		if(onWall && !OnGround && timeJumpHeld == maxJumpTime) {
			timeJumpHeld += 1;
			WallJump();
		}
		if(timeJumpHeld >= maxJumpTime) {
			return;
		}
		timeJumpHeld += Time.deltaTime;
		stepsSinceLastJump = 0;

		velocity.y = jumpSpeed;
	}

	void WallJump() {
		velocity = wallNormal * jumpSpeed * 1.5f;
		velocity.y = jumpSpeed * 1.2f;
	}

	void Hover() {
		if(hoverLength >= maxHoverLength) {
			particles.Stop();
			return;
		}
		if(!particles.isPlaying) {
			particles.Play();
		}
		hoverLength += Time.deltaTime;
		velocity.y = hoverSpeed;
	}

	void OnCollisionEnter (Collision collision) {
		EvaluateCollision(collision);
	}

	void OnCollisionStay (Collision collision) {
		EvaluateCollision(collision);
	}

	void EvaluateCollision (Collision collision) {
		float minDot = minGroundDotProduct;
		for (int i = 0; i < collision.contactCount; i++) {
			Vector3 normal = collision.GetContact(i).normal;
			if (normal.y >= minDot) {
				groundContactCount += 1;
				contactNormal += normal;
			}
			if((normal.y < 0.005 && normal.y > -0.005) && !onWall) {
				onWall = true;
				wallNormal = normal;
			}
		}
	}

	void IncreaseHover(float time) {
		maxHoverLength += time;
	}

	void SetRespawnPosition(Vector3 position) {
		respawnPosition = position;
	}

	void Respawn() {
		gameObject.transform.position = respawnPosition;
	}

	Vector3 ProjectOnContactPlane (Vector3 vector) {
		return vector - contactNormal * Vector3.Dot(vector, contactNormal);
	}
}
