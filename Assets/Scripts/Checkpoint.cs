using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {

    public Material activeMaterial;
    public Material inactiveMaterial;

    bool active = false;

    void OnTriggerEnter (Collider collider) {
        if(collider.gameObject.tag == "Player" && !active) {
            collider.gameObject.SendMessage("SetRespawnPosition", gameObject.transform.position);
            gameObject.transform.parent.BroadcastMessage("Deactivate");
            Activate();
        }
    }

    void Activate() {
        active = true;
        gameObject.GetComponent<Renderer>().material = activeMaterial;
    }

    void Deactivate() {
        active = false;
        gameObject.GetComponent<Renderer>().material = inactiveMaterial;
    }
}
